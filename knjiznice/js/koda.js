var baseUrl = 'https://rest.ehrscape.com/rest/v1';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

//GENERIRANJE VZORCNIH UPORABNIKOV
$(document).ready(function(){
    //klik na gumb za generiranje podatkov
    $('#generator').click(function(event){
        event.preventDefault();
        $.ajax({
            url: 'knjiznice/json/users.json',
            dataType: 'json',
            success: function(data){
               generirajPodatke(1, data, function(data1){
                    generirajPodatke(2, data, function(data2){
                        generirajPodatke(3, data, function(data3){
                            updateData([data1, data2, data3]);
                        });
                    });   
               }); 
            }
        });
    });
    
    $('#preberiObstojeciEHR').click(function(){
        $("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
    });
    
    pridobiLokacijo();
    
    $('#vadbe button').click(function(event){
      $('#kardio').addClass("hide");
      $('#moc').addClass("hide");
      $('#ogrevalne').addClass("hide");

      if(this.id == "kardio-button"){
        $('#kardio').removeClass('hide');
      }
      
      else if(this.id == "moc-button"){
        $('#moc').removeClass('hide');
      }
      
      else{
        $('#ogrevalne').removeClass('hide');
      }
    });
});

function updateData(list){
    var opt;
    for(i = 0; i < list.length; i++){
       // console.log(JSON.stringify(list));
        opt = document.getElementById('preberiObstojeciEHR').options[i];
        opt.value = list[i].ehrId;
        opt.text = list[i].firstName + ' ' + list[i].lastName;
   }
   
   document.getElementById('preberiEHRid').value = list[0].ehrId;
}

function generirajPodatke(patient_number, patient_data, callback) {
  patient_number--;

  var sessionId = getSessionId();
  var ehrId;
  var admin = "Damjan Kalsan";

  $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	$.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    success: function (data) {
      ehrId = data.ehrId;
      var params = {
          ehrId: ehrId,
          templateId: 'Vital Signs',
          format: 'FLAT',
          committer: admin
      };
      var patient_data_1 = {
          firstNames: patient_data[patient_number].firstName,
          lastNames: patient_data[patient_number].lastName,
          dateOfBirth: patient_data[patient_number].dateOfBirth,
          partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
      };

      //create new patient
      $.ajax({
        url: baseUrl + "/demographics/party",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(patient_data_1),
        success: function (data1) {
          if (data1.action == 'CREATE') {
            postData(0, function(){
              postData(1, function(){
                postData(2, function(json){
                    callback(json);
                });
              });
            });

            function postData(i, callback){
              var patient_data_2 = {
                  "ctx/language": "en",
                  "ctx/territory": "SI",
                  "ctx/time": patient_data[patient_number].vitalSigns[i].date,
                  "vital_signs/height_length/any_event/body_height_length": patient_data[patient_number].vitalSigns[i].height,
                  "vital_signs/body_weight/any_event/body_weight": patient_data[patient_number].vitalSigns[i].weight,
                  "vital_signs/body_temperature/any_event/temperature|magnitude": patient_data[patient_number].vitalSigns[i].temperature,
                  "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                  "vital_signs/blood_pressure/any_event/systolic": patient_data[patient_number].vitalSigns[i].bloodPressureSystolic,
                  "vital_signs/blood_pressure/any_event/diastolic": patient_data[patient_number].vitalSigns[i].bloodPressureDiastolic,
                  "vital_signs/indirect_oximetry:0/spo2|numerator": patient_data[patient_number].vitalSigns[i].oxygenSaturation
              };
              $.ajax({
                url: baseUrl + "/composition?" + $.param(params),
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(patient_data_2),
                success: function (data1) {
                  var json = {
                    firstName: patient_data[patient_number].firstName,
                    lastName: patient_data[patient_number].lastName,
                    ehrId: ehrId
                  };
                  callback(json);
                },
                error: function(err) {
                  console.error(err);
                }
              });
            }
          }
        },
        error: function(err) {
          console.error(err);
        }
      });
    },
    error: function(err){
      console.error(err);
    }
  });

  return ehrId;
}

function preberiPodatkeOUporabniku(){
    var ehrId = document.getElementById('preberiEHRid').value;
    var sessionId = getSessionId();
    
    readUserData(ehrId, function(data){
    //  console.log(JSON.stringify(data));
    var currentWeight = data.weight[data.weight.length - 1].weight;
    var currentHeight = data.height[data.height.length - 1].height;
    
    //  console.log(currentWeight);
    var currentBMI = (currentWeight/(currentHeight * currentHeight)) * 10000;
    currentBMI = currentBMI.toFixed(1);
    //console.log(currentBMI);
    
    document.getElementById('trenutniBMI').innerHTML = "Trenutni BMI znaša " + currentBMI;
    izrisiGraf(currentBMI);
    
    });
	
}

function readUserData(ehrId, callback){
  var json;
  var sessionId = getSessionId();
  
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/weight",
    type: 'GET',
    headers: {"Ehr-Session": sessionId},
    success: function(data1){
      $.ajax({
        url: baseUrl + "/view/" + ehrId + "/height",
        type: 'GET',
        headers: {"Ehr-Session": sessionId},
        success: function(data2){
          json = {
            weight: data1,
            height: data2
          }
          callback(json);
        },
        error: function(err){
          console.log(err);
        }
      });
    },
    error: function(err){
      console.log(err);
    }
  });
}

function pridobiLokacijo() {
  var x = document.getElementById("zemljevid");
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        alert("Vaš brskalnik ne podpira geolokacije. Funkcija zemljevida je izključena");
    }
    
  function showPosition(position) {
    x.src = "https://www.google.com/maps/embed/v1/search?key=AIzaSyCR0-XPIwiuRarDi1wZ2jUH46LCKw2lThg&q=fitness&center="+ position.coords.latitude +"," + position.coords.longitude + "&zoom=12";
  }
}

function izrisiGraf(currentBMI) {
    var Vzcontainer = document.getElementById('viz_container');
  if(Vzcontainer.firstChild) {
    Vzcontainer.removeChild(Vzcontainer.childNodes[0]);
    Vzcontainer.innerHTML = "<div id='viz_div' style='margin:0px auto; width:300px'></div>";
  }
  
  var wantedBMI;
  if(currentBMI < 18.5)
    wantedBMI = 18.5;
  else if(currentBMI > 24.9)
    wantedBMI = 24.9;
  else
    wantedBMI = currentBMI;
  
  var percentage = (currentBMI / wantedBMI) * 100;
  if(percentage > 100)
    percentage = 200 - percentage;
  
  var viz = vizuly.component.radial_progress(document.getElementById("viz_div"));

        viz.data(percentage)               // Current value
            .height(320)                    // Height/Diameter of component
            .min(0)                         // min value
            .max(100)                      // max value
            .capRadius(2)                   // Sets the curvature of the ends of the arc.
            .startAngle(220)                // Angle where progress bar starts
            .endAngle(140)                  // Angle where the progress bar stops
            .arcThickness(.12)              // The thickness of the arc (ratio of radius)
            .label(function (d,i) {         // Function used to create value label.
                return d3.format(".0f")(d) + "%";
            })
            .on("tween",onTween)            // Arc animation tween callback
            .on("mouseover",onMouseOver)    // mouseover callback
            .on("mouseout",onMouseOut)      // mouseout callback
            .on("click",onClick);           // click callback

        var vizTheme = vizuly.theme.radial_progress(viz)
                            .skin(vizuly.skin.RADIAL_PROGRESS_BUSINESS);

        viz.update();
}

function onTween(viz,i) {
        viz.selection().selectAll(".vz-radial_progress-label")
        .text(viz.label()(viz.data() * i));
    }

    function onMouseOver(viz,d,i) {
        //We can capture mouse over events and respond to them
    }

    function onMouseOut(viz,d,i) {
        //We can capture mouse out events and respond to them
    }

    function onClick(viz,d,i) {
        //We can capture click events and respond to them
    }